import React from 'react'
import  './App.css';
import { Navbar,Container,Nav,NavDropdown,NavLink } from 'react-bootstrap';

import Home from './pages/Home';
import Login from './pages/Login';
import Blog from './pages/Blog';
import Destinations from './pages/Destinations';
import TravelTips from './pages/TravelTips';
import About from './pages/About';
import Contact from './pages/Contact';
import Nopage from './pages/Nopage';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Footer from './pages/Footer';



function App() {
  return (
  <>

<div className='cursor-pointer'>
      <Navbar expand="lg" className="bg-black text-white gap-5 fixed-top  ">
      <Container>
       <Navbar.Brand href="Home"  className=' text-white text-bold h-10px fs-25' style={{alignItems:'flex-start',display:'flex'}}>Explore NatureZa</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav " style={{"color":"white"}}/>
        <Navbar.Collapse id="basic-navbar-nav">
          <div >
          <div className= ' d-flex justify-content-around'>
          <Nav className="me-auto bs-light text-white mx-5 gap-20">
            <Nav.Link href="Home"  className=' text-white margin-left:5px '>Home</Nav.Link>
           <Nav.Link href="Blog"  className=' text-white'>Blog</Nav.Link>
           <Nav.Link href="Destinations"  className=' text-white'>Destinations</Nav.Link>

          </Nav>
          </div>
          </div>
          <NavDropdown title="Categories" id="basic-nav-dropdown">
              <NavDropdown.Item href="./Components/Adventure">
                  Adventure
                </NavDropdown.Item>
                <NavDropdown.Item href="/Components/Mountains">
                  Mountains
                </NavDropdown.Item>
                <NavDropdown.Item href="/Components/Beaches">
                  Beaches
                </NavDropdown.Item>
                <NavDropdown.Item href="/Components/Wildlife">
                  Wildlife
                </NavDropdown.Item>
                <NavDropdown.Item href="/Components/Waterfalls">
                  Waterfalls
                </NavDropdown.Item>

              </NavDropdown>
              <Nav.Link href="TravelTips"  className=' text-white mx-2'>TravelTips</Nav.Link>

              <Nav.Link href="About"  className=' text-white mx-2'>About</Nav.Link>

            <Nav.Link href="Contact"  className=' text-white mx-2'>Contact</Nav.Link>
            
        </Navbar.Collapse>

      <Nav className="ml-auto flex-column display:flex" navbar >
          <NavLink>
            <Login />
          </NavLink>
      </Nav> 
      </Container>
    </Navbar>

</div>
<div className='mt-5'>
   <BrowserRouter>
   <Routes>
        <Route path='/Home' element={<Home />}/>
        <Route path="/Blog" element={<Blog />}/>
        <Route path="/Destinations" element={<Destinations />}/>
        <Route path="/TravelTips" element={<TravelTips />}/>
        <Route path="/About" element={<About />}/>
        <Route path="/Contact" element={<Contact />}/>
        <Route path="/Nopage" element={<Nopage />}/>
   </Routes>
   </BrowserRouter>
</div>


      <div className="content">
        <img src="https://mail.google.com/mail/u/0?ui=2&ik=c920e7b101&attid=0.5&permmsgid=msg-a:r440715546855083129&th=18c2152f4d990445&view=att&disp=safe&realattid=18c21513d7d966d5b121" alt="Your Photo" />
        <p>Your text content goes here. Customize this area with information about your journey, experiences, or anything you'd like to share with your audience.</p>
      </div>
    


<div>
  <Footer />
</div>
</>

  )
}

export default App;