
import React from 'react';
import { useNavigate } from 'react-router-dom';
import './About.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function About() {
    let navigate = useNavigate()
  return (
    <>
    <div ><img src="https://mail.google.com/mail/u/0?ui=2&ik=c920e7b101&attid=0.1&permmsgid=msg-a:r3815131720432419445&th=18c631d78fbac7c5&view=att&disp=inline&realattid=18c631cabd1db57cc401" alt="No Image Found"  style={{ width: '100%', height: '400px' }}/>
</div>
<div>

</div>
<section id="about-me">
  <h2 style={{color:"tomato"}}>About me</h2>
  <h3>Sindhura</h3>
 <p>Hello there! I'm Sindhura Chowdhary, and I'm truly passionate about capturing the beauty of the world through my lens. Photography, for me, is more than just a hobby – it's a way of expressing the emotions and stories that unfold in every frame.

</p>
<p>My love for exploration goes hand in hand with my photography journey. Traveling has opened my eyes to diverse cultures, landscapes, and perspectives. Through my lens, I aim to share the unique narratives I encounter during my adventures. From bustling cityscapes to serene natural wonders, each place has its own tale, waiting to be told.
</p>

<p>Blogging is another avenue through which I channel my creativity. I enjoy sharing not only my photographic work but also the experiences and insights gained during my travels. It's a platform where I hope to inspire others to embark on their own journeys and discover the beauty that exists in every corner of the world.

</p>
<p>Nature, in all its forms, has a special place in my heart. The serenity of a quiet forest, the grandeur of towering mountains, and the calming rhythm of the ocean – these elements fuel my appreciation for the world around us. Through my photography and words, I aspire to convey the importance of preserving and cherishing the natural wonders that surround us.
</p>

<p>Join me on this visual and narrative journey as I strive to capture the essence of life, one frame at a time. Thank you for being a part of my adventure!
</p>
</section >

 </>
  )
}

export default About;
