import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram,faYoutube } from '@fortawesome/free-brands-svg-icons';
import './Footer'


const Footer = () => {
  return (
    <footer>
 
      <div className='footer text-center text-white '>
        <br />
      <div className="icon-container column d-flex justify-content-center ">

<FontAwesomeIcon icon={faFacebook} size="1x" />
<FontAwesomeIcon icon={faTwitter} size="1x" />
<FontAwesomeIcon icon={faInstagram} size="1x" />
<FontAwesomeIcon icon={faYoutube} size="1x" />


{/* Add more icons as needed */}
</div>
      

    {/* <!-- Section: Form --> */}

    <br />
          <p >© Explore NatureZa 2023</p>
          <p  style={{color:"tomato",fontStyle:"italic"}}>Privacy Policy | Terms of Use</p>
          <br />
        </div>

    </footer>
  );
};

export default Footer;