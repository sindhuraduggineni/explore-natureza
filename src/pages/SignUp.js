
import React, { useState } from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';

function SignUp(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({
    firstname:'',
    lastname:'',
    email:'',
    password:'',
    country:''

})
const handleInput = (e) =>{
    const {name, value} = e.target
    setFormdata({
        ...formdata,
        [name]:value
    })
}
console.log(formdata)

 /* const toggle = (e) =>
  e.preventDefault();
   setModal(!modal);
};*/
  const toggle = (e) =>{
    e.preventDefault();
    setModal(!modal);
  
  }

  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      let response = await axios.post("http://localhost:3000/register",formdata)
     console.log(response)
     alert("data inserted")
     alert("Registered Successfully")
     setModal(!modal)

    }
     catch (err) {
      throw err
     }
    //alert("Successfully Done")


    }

  return (
    <div>
      <Button color="success" onClick={toggle}>
        Register 
      
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Registration Form</ModalHeader>
        <ModalBody>

<Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleFirstname">
        Firstname
        </Label>
        <Input
          id="exampleFirstname"
          name="firstname"
          placeholder="FirstName"
          type="Firstname"
          value={formdata.firstname}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleLastname">
        Lastname
        </Label>
        <Input
          id="exampleLastname"
          name="lastname"
          placeholder="LastName"
          type="lastname"
          value={formdata.lastname}
          onChange={handleInput}
          required

        />
      </FormGroup>
    </Col>
  </Row>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value={formdata.email}
          onChange={handleInput}
          required

        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="Password"
          type="password"
          value={formdata.password}
          onChange={handleInput}
          required

        />
      </FormGroup>
    </Col>
    </Row>
<Row>
    <FormGroup>
    <Label for="exampleCountry">
      Country
    </Label>
    <Input
                 id="examplecountry"
                 name="country"
                 placeholder="Country"
                 type="country"
                 value={formdata.country}
                 onChange={handleInput}
                 required

         />
  </FormGroup>
</Row>
{/*<FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Agree Terms and Conditions
    </Label>
  </FormGroup>*/}
    <a href='Register for new user!'></a>
  <Button type="submit">
    Register
    </Button>
    </Form>
 
    </ModalBody>

        
      </Modal>
    </div>
  );
}

export default SignUp;
