import React from 'react'
import './TravelTips.css';

function TravelTips() {
  return (
    <>
        <div ><img src="https://wallpapercave.com/wp/wp9689208.jpg" alt="No Image Found"  style={{ width: '100%', height: '400px' }}/>
</div>

    <br/>
    <br />
    <section id="travel-tips">
  <h2  className='text-center' style={{color:"tomato"}}>GET INSPlRED TO TRAVEL</h2>
  <br />
  <p>Embrace the essence of travel by immersing yourself in local culture, savoring regional cuisines, and engaging with the communities you encounter. Prioritize packing essentials, including versatile clothing and necessary documents. Stay flexible in your itinerary, allowing room for spontaneous discoveries. Invest in a good travel insurance plan and keep a digital backup of important documents. Respect local customs and traditions, and be open to the beauty of diverse perspectives. Lastly, remember to capture the moments but also to put down the camera and soak in the unique experiences that each destination offers.</p>


  <h2  className='text-center' style={{color:"tomato"}}>HOW TO SAVE FOR ANY TRIP</h2>
  <br />
<p>"Turning your travel dreams into reality begins with smart financial planning. Start by creating a dedicated savings fund for your adventures. Set a realistic budget by considering transportation, accommodation, meals, and activities. Cut unnecessary expenses and redirect those funds to your travel fund. Consider taking on a side hustle or finding ways to generate extra income. Look for travel deals and discounts, and be flexible with your travel dates to take advantage of cost-effective options. Prioritize your savings goal, and watch your travel fund grow as you make deliberate choices that bring you closer to your dream destination.

</p>
<h2  className='text-center' style={{color:"tomato"}}>HOW TO PLAN FOR A TRIP</h2>
  <br />
  <p>Planning a memorable trip starts with clear goals. Define your destination, set a budget, and create a flexible itinerary. Research extensively, considering local customs, weather, and key attractions. Book flights and accommodations well in advance for better deals. Pack smart by including essentials and versatile clothing. Stay organized with digital copies of important documents and a travel-friendly backpack. Embrace spontaneity but have a general plan to make the most of your time. Lastly, immerse yourself in the culture by learning a few local phrases and respecting the traditions of the places you visit.</p>





</section >
    </>
  )
}

export default TravelTips