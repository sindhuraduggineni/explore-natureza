import React from 'react';
import {Form, FormGroup, Label, Input,Row,Col } from 'reactstrap';


function Contact() {
  return (
  <>
    
    <div ><img src="https://as2.ftcdn.net/v2/jpg/02/64/51/31/1000_F_264513182_bvB3oizRZdkTe8wazQSLGAdu65Ed6RSY.jpg" alt="No Image Found"  style={{ width: '100%', height: '400px' }}/>
</div>

<br />
<div className='text-center'><h1 style={{color:'tomato'}}>GET IN TOUCH</h1></div>
<div className='text-center long-text'><h4 style={{fontFamily:'serif',fontWeight:'bold'}}>Contact form</h4></div>
<div className='mt-5'>
<div className=' container text-center mt-5'>
        <p className=' mt-5 fs-5'>Send me your questions, comments, or suggestions!</p>
        <p className='mt-3'> If you'd like to work with me or you have a question or comment, you can contact me using the form below.</p>
    </div>
      <div className='mt-5'>
        <center>
             <Form style={{width:'50%'}}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail"hidden>Name </Label>
        <Input
          name="name"
          placeholder="Name"
          type="text"
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
       
        <Input
          name="email"
          placeholder="Email"
          type="email"
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
   
    <Input
      name="phone"
      placeholder="Phone Number"
      required
    />
  </FormGroup>
  <FormGroup>
   
    <Input style={{height:'100px'}}
      name="message"
      placeholder="Message"
      required
          />
  </FormGroup>
  <p className='btn btn-warning'>Send</p>
  </Form></center>
 </div>
</div>


    
    
    </>
  )
}

export default Contact