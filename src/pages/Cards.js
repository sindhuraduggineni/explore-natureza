//import {useEffect,useState} from 'react'
//import axios from 'axios';
import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
function Cards(props) {
  const toggle = (e) => {
    e.preventDefault();
  }
    
  return (
    <>
    <div className='col-12 col-sm-4 col-md-3 ' style={{height:"400px" ,width:"400px" ,marginLeft:"10px",marginRight:"10px"  }}>
      
           <img src={props.data.Img} className='w-100  h-200 rounded-5 object-fit-cover '  style={{height:'200px'}} alt={props.data.Place}/>
           
           <div className='card-body'>
            <p className='text-dark text-center ' style={{fontWeight:'bold',fontSize:'20px'}}  onClick={toggle}>
              {props.data.Place}
              </p>
            <p className='text-bold  text-center' style={{fontWeight:'bold'}}>{props.data.Country}</p>
            <p className='text-center'>{props.data.Summary}</p>
            </div>
          
        </div>
        </>
  )
}
export default Cards;
