import React, { useState, useEffect } from 'react'
import { Carousel } from 'react-bootstrap';
import Cards from './Cards'
import axios from 'axios';
import '../App.js';
function Home(args) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3000/fetch")
        console.log(response.data)
        setData(response.data)

      } catch (err) {

      }
    };
    fetchData()
  }, []);
  return (
    <>
    
      <div>
        <Carousel className='fixed  mt-5'>
          <Carousel.Item>
            <img src="https://c4.wallpaperflare.com/wallpaper/764/431/702/river-trees-forest-clouds-wallpaper-preview.jpg" style={{ width: '100%', height: '400px' }} />

            <Carousel.Caption className='text-center'>

              <h1>Dream</h1>
              <p>Adventure Waiting...</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img src="https://c1.wallpaperflare.com/preview/460/412/978/beach-sea-holiday-waves.jpg" style={{ width: '100%', height: '400px' }} />

            <Carousel.Caption><center>
              <h1>Explore</h1>
              <p>Adventure Waiting...</p>
            </center>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img src="https://www.myglobalviewpoint.com/wp-content/uploads/2023/08/Most-Beautiful-Places-in-Greece-Featured-Image.jpg" style={{ width: '100%', height: '400px', }} />
            <Carousel.Caption><center>
              <h1>Discover</h1>
              <p>Adventure Waiting...</p>
            </center>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
        ᐧ</div>
      <div>
        <h1 className='text-info text-center text-black ' style={{fontSize:"50px"}}>Blog Posts</h1>
        <br/>
        <br />
        <div className='container'>
          <div className='row '>{data.map((e) => (

            <Cards data={e} />

          ))}
          </div>
        </div>
      </div>
    
    </>
  )
}

export default Home
