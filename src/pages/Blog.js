/*import React from 'react'
import { useNavigate } from 'react-router-dom';



function Blog() {
  
let navigate = useNavigate()

  return (
    <>
    <div>
        <button onClick={() =>{navigate('/')}}>Back to homepage</button>
    </div>
    <div>
<h1>Explore</h1>
    </div>
    </>
  )

    
  
}

export default Blog*/

import React, { useState, useEffect } from 'react'
import Cards from './Cards'
import axios from 'axios';
import '../App.js';

function Blog(args) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3000/fetch")
        console.log(response.data)
        setData(response.data)

      } catch (err) {

      }
    };
    fetchData()
  }, []);
  return (
    <>
    <div>
      <br />
      <br />
      <h2  className='text-center'  style={{fontSize:"35px",color:"tomato"}}>Discover your destination  in my Blog </h2>
      <br />
      <div ><img src="https://img.freepik.com/premium-photo/back-view-tourist-woman-with-hat-backpack-vacation-france-wanderlust-concept-photorea_931559-3728.jpg?size=626&ext=jpg&ga=GA1.1.1413502914.1696723200&semt=ais" alt="No Image Found"  style={{ width: '100%', height: '400px' }}/>
</div>

      <div className='text-center'>
     <p style={{textAlign:"center"}}>Embark on a journey of discovery with our travel blog as we unravel the enchanting tapestry of Your Destination. Venture off the beaten path to uncover hidden gems that add a unique allure to the destination.  Our blog is your guide to Your Destination, offering not just travel tips but a holistic experience, ensuring your journey is as memorable as the destination itself.</p>
    </div>
    </div>
      <div>
      
        <h1 className='text-info text-center text-black' style={{fontSize:"45px"}}>Travel Guides</h1><br/><br />
        <div className='container'>
          <div className='row '>{data.map((e) => (

            <Cards data={e} />

          ))}
          </div>
        </div>
      </div>
    
    </>
  )
}

export default Blog