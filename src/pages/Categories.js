import React, { useState, useEffect } from 'react'
import axios from 'axios';
import '../App.js';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Adventure from '../components/Adventure.js';
import Beaches from '../components/Beaches.js';
import Mountains from '../components/Mountains';
import Waterfalls from '../components/Waterfalls.js';
import Wildlife from '../components/Wildlife.js';

function Categories(args) {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await axios.get("http://localhost:3000/fetch")
        console.log(response.data)
        setData(response.data)

      } catch (err) {

      }
    };
    fetchData()
  }, []);
  return (
    <>
    <br />
    <div>Categories</div>
    <div>
      
      <h1 className='text-info text-center text-black' style={{fontSize:"45px"}}>Travel Guides</h1><br/><br />
      <div className='container'>
        <div className='row '>{data.map((e) => (

          <Categories data={e} />

        ))}
        </div>
        </div>
        </div>

        <div className='mt-5'>
   <BrowserRouter>
   <Routes>
        <Route path='./Adventure' element={<Adventure />}/>
        <Route path="/Beaches" element={<Beaches />}/>
        <Route path="/Mountains" element={<Mountains/>}/>
        <Route path="/Waterfalls" element={<Waterfalls />}/>
        <Route path="/Wildlife" element={<Wildlife/>}/>

   </Routes>
   </BrowserRouter>
</div>



    
    </>
  )
}

export default Categories